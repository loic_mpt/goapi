package goapi

import (
	"log"
	"net/http"
	"time"
)

type loggingResponseWriterWrapper struct {
	http.ResponseWriter
	statusCode int
}

func (w *loggingResponseWriterWrapper) WriteHeader(statusCode int) {
	w.ResponseWriter.WriteHeader(statusCode)
	w.statusCode = statusCode
}

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		s := time.Now()
		wrapper := &loggingResponseWriterWrapper{ResponseWriter: w, statusCode: http.StatusOK}
		next.ServeHTTP(wrapper, r)
		log.Println(wrapper.statusCode, r.Method, r.URL.Path, time.Since(s))
	})
}

type Middleware func(http.Handler) http.Handler

func CreateMiddlewareStack(ms ...Middleware) Middleware {
	return func(next http.Handler) http.Handler {
		for i := len(ms) - 1; i >= 0; i-- {
			next = ms[i](next)
		}
		return next
	}
}
