package goapi

import (
	"net/http"
	"slices"
)

type MemoryAuthorizationMiddleware struct {
	Authorizations map[string][]string
}

func NewMemoryAuthorizationMiddleware() *MemoryAuthorizationMiddleware {
	return &MemoryAuthorizationMiddleware{
		Authorizations: make(map[string][]string),
	}
}

// REQUIRE AUTHENTICATION BEFORE
func (m *MemoryAuthorizationMiddleware) Authorize(profile string) Middleware {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user, ok := r.Context().Value(CTX_USERNAME).(string)
			if ok && slices.Contains(m.GetUserAuthorizations(user), profile) {
				next.ServeHTTP(w, r)
				return
			}
			http.Error(w, "Forbidden", http.StatusForbidden)
		})
	}
}

func (m *MemoryAuthorizationMiddleware) GetUserAuthorizations(username string) []string {
	if authorizations, ok := m.Authorizations[username]; ok {
		return authorizations
	}
	return make([]string, 0)
}

func (m *MemoryAuthorizationMiddleware) WithAuthorizations(user string, authorizations []string) *MemoryAuthorizationMiddleware {
	m.Authorizations[user] = authorizations
	return m
}
