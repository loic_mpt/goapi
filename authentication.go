package goapi

import (
	"context"
	"crypto/sha256"
	"fmt"
	"net/http"
)

const CTX_USERNAME = "CTX_USERNAME"

type AuthenticationInterface interface {
	Authenticate(next http.Handler) http.Handler
}

var _ AuthenticationInterface = &BasicAuthenticationMiddleware{}

type BasicAuthenticationMiddleware struct {
	Users map[string]string
}

func NewBasicAuthenticationMiddleware() *BasicAuthenticationMiddleware {
	return &BasicAuthenticationMiddleware{
		Users: make(map[string]string),
	}
}

func (b *BasicAuthenticationMiddleware) Authenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if username, password, ok := r.BasicAuth(); ok {
			if b.IsValid(username, password) {
				// ajoute l'identifiant de l'utilisateur dans le context de la requete
				r = r.WithContext(context.WithValue(r.Context(), CTX_USERNAME, username))
				next.ServeHTTP(w, r)
				return
			}
		}
		w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
	})
}

func (b *BasicAuthenticationMiddleware) WithCrendentials(user, password string) *BasicAuthenticationMiddleware {
	b.Users[user] = fmt.Sprintf("%x", sha256.Sum256([]byte(password)))
	return b
}

func (b *BasicAuthenticationMiddleware) IsValid(user, password string) bool {
	h, ok := b.Users[user]
	return ok && h == fmt.Sprintf("%x", sha256.Sum256([]byte(password)))
}
