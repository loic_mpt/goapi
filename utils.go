package goapi

import (
	"encoding/json"
	"encoding/xml"
	"net/http"
)

type ResponseWriterFunc func(w http.ResponseWriter, code int, data any)

func WriteJson(w http.ResponseWriter, code int, data any) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(data)
}

func WriteXml(w http.ResponseWriter, code int, data any) {
	w.Header().Set("Content-Type", "application/xml")
	w.WriteHeader(code)
	xml.NewEncoder(w).Encode(data)
}

func ErrorMessage(m string) map[string]string {
	return map[string]string{
		"error": m,
	}
}
