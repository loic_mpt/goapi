package goapi

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

var user, pass = "loic", "test"

func toJson(v any) string {
	b, _ := json.Marshal(v)
	return string(b)
}

func compareJsonString(a, b string) bool {
	var ja, jb interface{}
	if err := json.Unmarshal([]byte(a), &ja); err != nil {
		return false
	}
	if err := json.Unmarshal([]byte(b), &jb); err != nil {
		return false
	}
	return reflect.DeepEqual(jb, ja)
}

func TestJsonResponse(t *testing.T) {
	var expectedJson = map[string]string{"message": "faire des crepes"}

	// build server
	smux := http.NewServeMux()
	smux.HandleFunc("GET /json", func(w http.ResponseWriter, r *http.Request) {
		WriteJson(w, http.StatusOK, expectedJson)
	})

	// build request
	request, err := http.NewRequest("GET", "/json", nil)
	if err != nil {
		t.Fatalf("Error when building request %v", err)
	}

	// exec request
	resp := httptest.NewRecorder()
	smux.ServeHTTP(resp, request)

	// check result
	if resp.Code != 200 {
		t.Fatalf("Response code '%d' != 200", resp.Code)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Error when reading body %v", err)
	}

	if !compareJsonString(toJson(expectedJson), string(body)) {
		t.Fatalf("Invalide json response: %s", string(body))
	}
}

func TestAuthentication(t *testing.T) {
	// build server
	smux := http.NewServeMux()
	smux.HandleFunc("GET /test", func(w http.ResponseWriter, r *http.Request) {
		WriteJson(w, http.StatusOK, map[string]string{"a": "b"})
	})

	srv := CreateMiddlewareStack(
		NewBasicAuthenticationMiddleware().WithCrendentials(user, pass).Authenticate,
	)(smux)

	// build request
	request, err := http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatalf("Error when building request %v", err)
	}

	// exec request
	resp := httptest.NewRecorder()
	srv.ServeHTTP(resp, request)

	// check result
	if resp.Code != 401 {
		t.Fatalf("Response code '%d' != 401", resp.Code)
	}

	// build request with auth
	request, err = http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatalf("Error when building request %v", err)
	}
	request.Header.Add("Authorization", "Basic "+base64.StdEncoding.EncodeToString([]byte(user+":"+pass)))

	// exec request
	resp = httptest.NewRecorder()
	srv.ServeHTTP(resp, request)

	// check result
	if resp.Code != 200 {
		t.Fatalf("Response code '%d' != 200", resp.Code)
	}
}

func TestAuthorization(t *testing.T) {
	var unauthorizedUser, unauthorizedPass = "denis", "toto"
	// build server
	smux := http.NewServeMux()
	smux.HandleFunc("GET /test", func(w http.ResponseWriter, r *http.Request) {
		WriteJson(w, http.StatusOK, map[string]string{"user": "b"})
	})

	srv := CreateMiddlewareStack(
		NewBasicAuthenticationMiddleware().WithCrendentials(user, pass).WithCrendentials(unauthorizedUser, unauthorizedPass).Authenticate,
		NewMemoryAuthorizationMiddleware().WithAuthorizations(user, []string{"admin"}).WithAuthorizations(unauthorizedUser, []string{"user"}).Authorize("admin"),
	)(smux)

	values := map[string]int{
		user + ":" + pass:                         200,
		unauthorizedUser + ":" + unauthorizedPass: 403,
	}

	for userPass, code := range values {
		// build request
		request, err := http.NewRequest("GET", "/test", nil)
		if err != nil {
			t.Fatalf("Error when building request %v", err)
		}
		request.Header.Add("Authorization", "Basic "+base64.StdEncoding.EncodeToString([]byte(userPass)))

		// exec request
		resp := httptest.NewRecorder()
		srv.ServeHTTP(resp, request)

		// check result
		if resp.Code != code {
			t.Fatalf("Response code '%d' != %d", resp.Code, code)
		}
	}
}
